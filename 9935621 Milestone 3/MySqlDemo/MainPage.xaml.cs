﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using MySql.Data.MySqlClient;
using MySQLDemo.Classes;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MySqlDemo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            
            names.ItemsSource = loadAllTheData();

        }
        
        
        

        void selectedName(string name)
        {
            var command = $"Select * from tbl_people WHERE FNAME = '{name}'; ";
            var b = MySQLCustom.ShowInList(command);

            userAccount.Text = b[0];
            fnametxt.Text = b[1];
            lnametxt.Text = b[2];
            dobtxt.Text = b[3];
            stNumber.Text = b[4];
            stName.Text = b[5];
            postcodetxt.Text = b[6];
            citytxt.Text = b[7];
            phone1txt.Text = b[8];
            phone2txt.Text = b[9];
            emailtxt.Text = b[10];

            
        }

        static List<string> loadAllTheData()
        {
            
            
            //Set the command and executes it and returns a list
            var command = $"Select FNAME from tbl_people";

            //Call the custom method
            var list = MySQLCustom.ShowInList(command);

            //Display the list (in this case a combo list)
            return list;
        }

        
        

        void clearAll()
        {
            fnametxt.Text = "";
            lnametxt.Text = "";
            dobtxt.Text = "";
            stNumber.Text = "";
            stName.Text = "";
            postcodetxt.Text = "";
            citytxt.Text = "";
            phone1txt.Text = "";
            phone2txt.Text = "";
            emailtxt.Text = "";
            userAccount.Text = "";
        }

        static async void messageBox(string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = "Checking value";

            dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            var res = await dialog.ShowAsync();

            if ((int)res.Id == 0)
            { }
        }

        //Below are the object methods

        private void names_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Checks if the object still exists
            if (names.SelectedItem != null)
            {
                selectedName(names.SelectedItem.ToString());
            }
        }

        
        private void Addbtn_Click(object sender, RoutedEventArgs e)
        {
            //Add Information to the Database
            //                          1           2               3         4          5               6                   7           8               9           10 
            int stNumberParse = int.Parse(stNumber.Text);
            MySQLCustom.AddData(fnametxt.Text, lnametxt.Text, dobtxt.Text, stNumberParse, stName.Text, postcodetxt.Text, citytxt.Text, phone1txt.Text, phone2txt.Text, emailtxt.Text);
            names.ItemsSource = loadAllTheData();
            clearAll();
        }

        private void Updatebtn_Click(object sender, RoutedEventArgs e)
        {
            //Update Information in the Database
            //                              1           2               3           4               5               6              7           8               9           10                   11
            int stNumberParse = int.Parse(stNumber.Text);
            MySQLCustom.UpdateData(fnametxt.Text, lnametxt.Text, dobtxt.Text, stNumberParse, stName.Text,  postcodetxt.Text, citytxt.Text, phone1txt.Text, phone2txt.Text, emailtxt.Text, userAccount.Text);
            names.ItemsSource = loadAllTheData();
            clearAll();
        }

        private void Deletebtn_Click(object sender, RoutedEventArgs e)
        {
            //Remove Information from the Database
            MySQLCustom.DeleteDate(userAccount.Text);
            names.ItemsSource = loadAllTheData();
            clearAll();
        }


        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            Splitter.IsPaneOpen = !Splitter.IsPaneOpen;
            

        }



        // ********************Button THAT SHRINKS THE FIRST COLUMN *********************
        private void toggleButton_Click(object sender, RoutedEventArgs e)
        {

            if (toggleButton.IsChecked == false)
            {
                Column1st.Width = new GridLength(0, GridUnitType.Star);
                Row1st.Height = new GridLength(0, GridUnitType.Star);


            }

            else if(toggleButton.IsChecked == true)
            {
                Column1st.Width = new GridLength(1, GridUnitType.Star);
                Row1st.Height = new GridLength(1, GridUnitType.Star);
            }
            


        }


    }
}
